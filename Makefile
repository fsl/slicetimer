include $(FSLCONFDIR)/default.mk

PROJNAME = slicetimer
XFILES   = slicetimer
LIBS     = -lfsl-newimage -lfsl-miscmaths -lfsl-NewNifti \
           -lfsl-znz -lfsl-cprob -lfsl-utils

all: ${XFILES}

slicetimer: slicetimer.o
	${CXX}  ${CXXFLAGS} -o $@  $^ ${LDFLAGS}
